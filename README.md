PhoneGapTest
================================

[PhoneGapTestRun](https://bitbucket.org/hankei6km/phonegaptestrun)ライブラリを試しみるためのEclipse+AndoridSDKなプロジェクト

利用方法
---------

1. Eclipseの同一ワークスペース上に、このプロジェクトとPhoneGapTestRunライブラリをインポートする。

2. インポートをした後にEclipse上のメニューで `Project/Clean` を選択して、インポートした両プロジェクトのCleanupを行う(このとき `gen` が無いというようなエラーが出たら、エクスプローラーなどでプロジェクトのルートに `gen` フォルダを作成し何度かCleanを実行する)。

3. インポートしたプロジェクトそれぞれを右クリックし、 `Android Tools/Fix Project Properties` を選択する(これでプロジェクトのエラーが消えるはず)。

4. このプロジェクトをのプロパティからAndroidタブを開き、Library一覧に `PhoneGapTestRun` プロジェクトを追加する。

5. このプロジェクトをAndroid Applicationとして実行し、画面にPhoneGapTestRun NormalStartと表示れることを確認する。

6. PC上のコマンドプロンプトから `adb shell am start -n com.fc2.blog23.zawakei.test.phonegaptest/com.fc2.blog23.zawakei.test.phonegaptestrun.Test` を実行する。 

7. Android上にQUnitの実行画面が表示されたら確認は終了(テストの実行結果はわざと失敗するようになっています)。

備考
----

詳細は「[PhoneGap上でテストを実行する方法を考えてみる](http://zawakei.blog23.fc2.com/blog-entry-346.html)」を参照。